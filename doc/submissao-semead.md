# Objetivo

Um maior enfoque na etapa de geração de alternativas pode mostrar qual a contribuição que a utilização de um método estruturado sobre o processo decisório como um todo teria a oferecer para a qualidade das alternativas. No caso do presente trabalho, deseja-se investigar como um processo de implantação do AHP interfere como um agente condicionante e motivador para que equipes gerenciais possam obter um melhor conjunto de alternativas.

# Método

A fim de analisar a geração de alternativas na implantação de métodos estruturados de decisão, aproveitou-se o estudo de caso de 5 empresas que realizaram a priorização de projetos estratégicos e de pesquisa e desenvolvimento utilizando o AHP. Nestes estudos de caso procurou-se analisar as etapas de levantamento, apresentação, descarte, priorização, consenso, busca e geração de alternativas.

# Fundamentação Teórica

Diversos autores têm enfatizado com grande aceitação o processo decisório como um fator crucial na qualidade das decisões, sendo a etapa de geração de alternativas um importante indicador de qualidade. Mesmo assim, entende-se que esta é uma etapa crítica, pouco estudada, e que exige a criação de mecanismos que favoreçam o exercício da criatividade. Embora o AHP formalmente não tenha este enfoque, acredita-se que sua utilização auxiliem os gestores a organizar, e então gerar mais alternativas.

# Considerações Finais

Com o cenário analisado, embora o AHP não seja diretamente aplicável quando uma organização precisa gerar alternativas frente à um problema, existem subprodutos bastante interessantes que inferem no conjunto de alternativas , sendo observado que o  método apresenta-se como um mediador que estimula a qualidade das alternativas e a comunicação das organizações.

# Contribuições

Tomando a importância do conjunto de alternativas frente à qualidade de um processo decisório, os estudos acerca do AHP poderiam envolver a etapa de alternativas com mais frequência, portanto espera-se que o presente estudo tenha contribuído para o entendimento da etapa de geração de alternativas, e valorizado os métodos de estruturação do processo decisório como importantes ferramentas de integração e gestão nas organizações.
